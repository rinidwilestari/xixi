<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Soal 1 'Menunjukkan panjang dari string dan jumlah kata'</h1>
    <?php
    $kalimat = "PHP is never old";
    echo "$kalimat <br>";
    echo "Panjang string :" . strlen($kalimat) . "<br>";
    echo "Jumlah kata :" . str_word_count($kalimat) . "<br>"
    ?>

<h1>first sentence "Hello PHP!"</h1>
   <?php
    $first_sentence = "Hello PHP!";
echo "$first_sentence <br>";
    echo "Panjang string :" . strlen($first_sentence) . "<br>";
    echo "Jumlah kata :" . str_word_count($first_sentence) . "<br>";
    ?>

<h2>second_sentence = "I'm ready for the challenges"</h2>
<?php
$second_sentence = "I'm ready for the challenges";
echo "$second_sentence <br>";
echo "Panjang string :" . strlen($second_sentence) . "<br>";
echo "Jumlah kata :" . str_word_count($second_sentence) . "<br> <br>";
?>

<h1>Soal 2 'Mengambil kata pada string dan karakter-karakter yang ada di dalamnya'</h1>
<?php
 $string2 = "I love PHP";
 echo "Kalimat : $string2 <br>";
 echo "Kata Pertama :" . substr($string2, 0, 1) . "<br>";
 echo "Kata Kedua :" . substr($string2, 2, 4) . "<br>";
 echo "Kata Ketiga :" . substr($string2, 6, 4) . "<br>";

?>
 
<h1> Soal 3 'Mengubah karakter atau kata yang ada di dalam sebuah string'</h1>
<?php 
$string3 = "PHP is old but sexy!";
echo "Kalimat : $string3 <br>";
echo "Kalimat di ganti :" . str_replace("sexy","awesome", $string3);
?>
</body>
</html>
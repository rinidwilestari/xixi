<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Kelompokkan nama-nama di bawah ini ke dalam Array</h1>
    <?php
    $Kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"] ;
    $Adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
print_r($Kids);
echo "<br>";
echo "Total Kids :" . count($Kids);
echo "<ul>";
echo "<li>" . $Kids[0]. "</li>";
echo "<li>" . $Kids[1]. "</li>";
echo "<li>" . $Kids[2]. "</li>";
echo "<li>" . $Kids[3]. "</li>";
echo "<li>" . $Kids[4]. "</li>";
echo "<li>" . $Kids[5]. "</li>";
echo "</ul>";
print_r($Adults);
echo "<br>";
echo "Total Adults :" . count($Adults);
echo "<ul>";
echo "<li>" . $Adults[0]. "</li>";
echo "<li>" . $Adults[1]. "</li>";
echo "<li>" . $Adults[2]. "</li>";
echo "<li>" . $Adults[3]. "</li>";
echo "<li>" . $Adults[4]. "</li>";
echo "</ul>";
    ?>

    <h1>Soal 2 tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array</h1>
    <?php
$Kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"] ;
$Adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
echo "Cast Stranger Things: ";
print_r($Kids);
    echo "<br>";
    $jml_kids = count($Kids);
    echo "Panjang array kids  :" . $jml_kids; // Berapa panjang array kids
    echo "<br>";
    echo "<ol>";
    echo "<li> $Kids[0] </li>";
    echo "<li> $Kids[1] </li>";
    echo "<li> $Kids[2] </li>";
    echo "<li> $Kids[3] </li>";
    echo "<li> $Kids[4] </li>";
    echo "<li> $Kids[5] </li>";
    ?>
    
<h1>Soal 3 Susun data-data berikut ke dalam bentuk Asosiatif Array didalam Array Multidimensi</h1>
  <?php
  $nama = ["Will Byers", "Mike Wheeler", "Jim Hopper", "Eleven"];
  print_r($nama);
  echo "<br>";
  echo "<ul>";
  echo "<li>" . $nama[0]. "</li>";
  echo "<li>" . $nama[1]. "</li>";
  echo "<li>" . $nama[2]. "</li>";
  echo "<li>" . $nama[3]. "</li>";
  echo "</ul>";

  $data = [
      [
          "name" => "Will Byers", "Age" => 12, "Aliases" => "Will the wise", "Status" => "Alive"
      ],
      [
        "name" => "Mike Wheeler", "Age" => 12, "Aliases" => "Dungeon Master", "Status" => "Alive"
    ],
    [
        "name" => "Jim Hopper", "Age" => 43, "Aliases" => "Chief Hopper", "Status" => "Deceased"
    ],
    [
        "name" => "Eleven", "Age" => 12, "Aliases" => "El", "Status" => "Alive"
    ]
  ];
  echo "<pre>";
print_r($data);
echo "</pre>";
  ?>
</body>
</html>